<?php
namespace Engaged\Instantdrillo\Tests;

use Engaged\Instantdrillo\Exception\ValidationError;

use Engaged\Instantdrillo\Exception\ValidationException;

use Engaged\Instantdrillo\Core\PlayResult;
use Engaged\Instantdrillo\Core\Prize;
use Engaged\Instantdrillo\Exception\WrongFormatException;

class EntityTest extends AbstractTest
{

	public function testPrize()
    {
		$json = '{"name":"prize name","qty":"100"}';
		$prize = new Prize($json);
		$this->assertEquals("prize name", $prize->getName());
	}

	public function testPrizeAndFailBecauseJsnoNotCorrect()
    {
		$json = 'this format is not json';
		$wrongFormat = false;
		try {
			$prize = new Prize($json);
		} catch (WrongFormatException $e) {
			$wrongFormat = true;
		}

		$this->assertEquals(true, $wrongFormat);
	}


	public function testPlayResultEmpty()
    {
		$playResult = new PlayResult();
		$this->assertEquals($playResult->isWin(), null);
		$this->assertEquals($playResult->getPrize(), null);
	}

	public function testPlayResultNotWinning()
    {
		$json = '{"win":"false","resultCode":"666","result":"Beast number"}';
		$playResult = new PlayResult($json);
		$playResult->isWin();
		$this->assertEquals($playResult->isWin(), false);
		$this->assertEquals($playResult->getPrize(), null);
		$this->assertEquals($playResult->getResultCode(), "666");
		$this->assertEquals($playResult->getResult(), "Beast number");
		$this->assertEquals($playResult->getJson(), $json);
		
	}

	public function testPlayResultWinning()
    {
		$json = '{"win":"true","prize":{"name":"prize name"},"resultCode":"666","result":"Beast number"}';
		$playResult = new PlayResult($json);

		$this->assertEquals($playResult->isWin(), true);
		$this->assertNotEquals($playResult->getPrize(), null);
		$this->assertEquals($playResult->getPrize()->getName(), "prize name");
		$this->assertEquals($playResult->getResultCode(), "666");
		$this->assertEquals($playResult->getResult(), "Beast number");
		$this->assertEquals($playResult->getJson(), $json);
	}


	public function testValidationExceptionWithOneError()
    {
		$json = '[{"errorMessage":"gameToken \'wrongGameToken\' is disabled or not registered","errorCode":"502","errorField":"gameToken"}]';
		$validationException = new ValidationException($json);

		$this->assertEquals(1, sizeof($validationException->getErrors()));
		$error = $validationException->getErrors()[0];
		$this->assertEquals(502, $error->getErrorCode());
		$this->assertEquals("gameToken", $error->getErrorField());
		$this->assertEquals("gameToken 'wrongGameToken' is disabled or not registered", $error->getErrorMessage());
	}

	public function testValidationExceptionWithTwoErrors()
    {
		$json = '[{"errorMessage":"gameToken \'wrongGameToken\' is disabled or not registered","errorCode":"502","errorField":"gameToken"},'.
					'{"errorMessage":"second error","errorCode":"100","errorField":"noField"}]';
		$validationException = new ValidationException($json);

		$this->assertEquals(2, sizeof($validationException->getErrors()));
		$error1 = $validationException->getErrors()[0];
		$this->assertEquals(502, $error1->getErrorCode());
		$this->assertEquals("gameToken", $error1->getErrorField());
		$this->assertEquals("gameToken 'wrongGameToken' is disabled or not registered", $error1->getErrorMessage());

		$error2 = $validationException->getErrors()[1];
		$this->assertEquals(100, $error2->getErrorCode());
		$this->assertEquals("noField", $error2->getErrorField());
		$this->assertEquals("second error", $error2->getErrorMessage());
	}
	
	
	

}
