Instantdrillo PHP client lib 
============================

Install
-------
Use this `composer.json` in your project
```json
{

	"repositories": [
	    {
	        "type":"package",
	        "package": {
	          "name": "instantdrillo-php-client",
	          "version":"dev",
	          "source": {
	              "url": "https://bitbucket.org/engaged/instantdrillo-php-client.git",
	              "type": "git",
	              "reference":"dev"
	            }
	        }
	        
	    }
	],
	"require": {
	    "php": ">=5.3.0",
	    "instantdrillo-php-client": "dev",	           
        "guzzle/guzzle": "~3.7"
	},
	
	 "autoload": {
     		   "psr-0": {                   	 
            		"Engaged": "vendor/instantdrillo-php-client/src/"
        		}
    		}


}

```

Usage
-----

If you want to change url to local or to test APIs you can specify the alternative url to the instantdrillo on Instantdrillo::getInstance($baseUrl, $gameToken) , making this configuration stuff more clean or environment dependent is up to you.


```php
$alreadyWon = false;
$instantdrillo = new Instantdrillo('https://therest.api.entrypoint.com/v1/','your_secret_game_token');

$result = null; 
try {
 	$result = $instantdrillo->play("aUserToken");
} catch (ValidationException $e) {
	// arrive here only for gameToken wrong or userToken missing
	$errors = $e->getErrors();
	echo "validation errors in json: ".$e->getJson();	
} 


echo "result message:".$result->getResult()."\n";
echo "result code:".$result->getResultCode()."\n";
...


``` 
