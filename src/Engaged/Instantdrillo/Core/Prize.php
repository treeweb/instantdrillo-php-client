<?php
/*
 Copyright (c) 2013 Engaged srl <info@engaged.it>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

namespace Engaged\Instantdrillo\Core;

use Engaged\Instantdrillo\Common\StringUtils;

class Prize
{

	const NAME = "name";

	private $name;
	
	private $json;

	public function __construct($json=null)
    {
    	$this->json = $json;
    	
		if ($json==null)
			return;

		// TODO capire cosa fare se il json è formattato male
		$dataAsArray = StringUtils::jsonToArray($json);
		$this->_initFromArray($dataAsArray);
	}

	public function initFromArray($dataAsArray)
    {
		$this->_initFromArray($dataAsArray);
	}

	/**
	 * @return string
	 */
	public function getName()
    {
		return $this->name;
	}
	
	/**
	 * @return string
	 */
	public function getJson() {
		return $this->json;
	}


	///////// private stuffff  ////////
	private function _initFromArray($dataAsArray)
    {
		$this->name = $dataAsArray[Prize::NAME];
	}
}
