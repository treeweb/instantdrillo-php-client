<?php
/*
Copyright (c) 2013 Engaged srl <info@engaged.it>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

namespace Engaged\Instantdrillo;

class Constants
{

	//////// validation
	// framework
	const VALIDATION_ERROR_CODE_MANDATORY = "51";
	const VALIDATION_ERROR_CODE_TYPE = "52";
	const VALIDATION_ERROR_CODE_TYPE_DONT_KNOW = "53";
	const VALIDATION_ERROR_CODE_FORMAT = "54";
	const VALIDATION_ERROR_CODE_PATERN = "55";
	const VALIDATION_ERROR_CODE_UNIQUE_VIOLATION = "56";
	const VALIDATION_ERROR_CODE_READ_ONLY_VIOLATION = "57";
	// specific	
	const VALIDATION_ERROR_CODE_ALREADY_PLANNED = "501";
	const VALIDATION_ERROR_CODE_WRONG_GAME_TOKEN = "502";

}
