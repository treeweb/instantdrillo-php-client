<?php

namespace Engaged\Instantdrillo;


/**
 *
 * Declaring InstantdrilloInterface
 *
 **/
interface InstantdrilloInterface
{
    /**
    * @param string $userToken - identify one and only one user
    * @return PlayResult
    **/
    public function play($userToken);
}
