<?php
/*
 Copyright (c) 2013 Engaged srl <info@engaged.it>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

namespace Engaged\Instantdrillo;

use Engaged\Instantdrillo\Exception\ValidationException;

use Guzzle\Http\Exception\ClientErrorResponseException;

use Guzzle\Http\Exception\BadResponseException;

use Engaged\Instantdrillo\Core\PlayResult;

use Guzzle\Http\Client;

class Instantdrillo implements InstantdrilloInterface
{

	private $baseUrl;
	private $gameToken;

	/**
	 * @var Client
	 */
	private $restClient;
	
	public function __construct($baseUrl , $gameToken)
	{
		$this->setBaseUrl($baseUrl)
		->setGameToken($gameToken);
	
		$this->restClient = new Client($this->getBaseUrl());
	}

	private function setGameToken($gameToken)
    {
		$this->gameToken = $gameToken;
		return $this;
	}

	private function setBaseUrl($baseUrl)
    {
		$this->baseUrl = $baseUrl;
		return $this;
	}

	public function getBaseUrl()
    {
		return $this->baseUrl;
	}

	public function getGameToken()
    {
		return $this->gameToken;
	}

	/**
	 * Perform a play call for a specific user
	 * @param string $userToken
	 * @return PlayResult
	 */
	public function play($userToken)
    {

		$bodyJson = '{"gameToken":"'.$this->gameToken.'", "userToken":"'.$userToken.'"}';
		$request = $this->restClient->post("play", array(), $bodyJson);
		$response=null;

		try {

			$response = $request->send();

		} catch (ClientErrorResponseException $e) {

			if ($e->getResponse()->getStatusCode()==400) {
				$validationException = new ValidationException($e->getResponse()->getBody(true));
				throw $validationException;
			}

		}
		return new PlayResult($response->getBody(true));
	}


		
	
}
